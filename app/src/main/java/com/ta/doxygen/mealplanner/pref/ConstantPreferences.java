package com.ta.doxygen.mealplanner.pref;

public class ConstantPreferences {
    public static final String NAMA = "NAMA";
    public static final String EMAIL = "EMAIL";
    public static final String UMUR = "UMUR";
    public static final String GENDER = "GENDER";
    public static final String IMAGE = "IMAGE";
    public static final String IMAGE_KEY = "IMAGE_KEY";
    public static final String KEY = "Preferences";
    public static final String TOKEN = "TOKEN";
    public static final String TOKEN_KEY = "1";
    public static final String AKTIVITAS = "AKTIVITAS";
    public static final String WEIGHT = "Weight";
    public static final String HEIGHT = "Height";
    public static final String KURUS = "Kurus";
    public static final String GEMUK = "Gemuk";
    public static final String NORMAL = "Normal";
    public static final String SANGAT_GEMUK = "Sangat Gemuk";
    public static final String MALNUTRISI = "Malnutrisi";
    public static final String IDEAL = "Ideal";
    public static final String OBESITAS = "Obesitas";
    public static final String GOLONGAN = "Golongan";
    public static final String ERROR_SERVER = "Server Error";
    public static final String ERROR_DATA = "Invalid Username or Password";
}
