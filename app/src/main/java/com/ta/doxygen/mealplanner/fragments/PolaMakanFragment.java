package com.ta.doxygen.mealplanner.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ta.doxygen.mealplanner.R;
import com.ta.doxygen.mealplanner.model.Rule;
import com.ta.doxygen.mealplanner.pref.ConstantPreferences;
import com.ta.doxygen.mealplanner.pref.PreferenceHelper;

import java.text.DecimalFormat;

public class PolaMakanFragment extends Fragment {
    private PreferenceHelper preferenceHelper;
    private TextView tvImt, tvKategori, tvKalori;
    private double imtValue;
    private Double bmr;
    private String message;
    private Button bCekKategori, bMenuDiet;
    private TextView tvKondisi;
    private String args;
    private Rule rule;
    DecimalFormat df2 = new DecimalFormat(".##");

    public PolaMakanFragment(){

    }

    public static PolaMakanFragment newInstance(String value) {

        Bundle args = new Bundle();
        args.putString("aktivitas", value);
        PolaMakanFragment fragment = new PolaMakanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_polamakan, container, false);;
        tvImt =  view.findViewById(R.id.tv_imt);
        tvKategori = view.findViewById(R.id.tv_kategori);
        bCekKategori = view.findViewById(R.id.b_cek_kategori);
        tvKondisi = view.findViewById(R.id.tv_kondisi);
        tvKalori = view.findViewById(R.id.tv_kalori);
        bMenuDiet = view.findViewById(R.id.btnmenudiet);
        getActivity().setTitle("Pola Makan");
        setupSharedPref();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        args = getArguments().getString("aktivitas");
        rumusImt();
        valueBMI(preferenceHelper.getString(ConstantPreferences.GENDER, "gender"));
        getKalori();
        bCekKategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog();
            }
        });
        bMenuDiet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rule = new Rule(getContext(), tvKondisi.getText().toString(), preferenceHelper.getInt(ConstantPreferences.GOLONGAN, 0));
                preferenceHelper.setString(ConstantPreferences.IMAGE_KEY, String.valueOf(rule.getImage()));
                Fragment fragment = new MenuDietFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

    private void rumusImt(){
        double beratBadan = Double.parseDouble(preferenceHelper.getString(ConstantPreferences.WEIGHT, "0"));
        double tinggiBadan = Double.parseDouble(preferenceHelper.getString(ConstantPreferences.HEIGHT, "0"));
        imtValue = (beratBadan/(tinggiBadan*tinggiBadan))*10000;
        tvImt.setText(String.valueOf(df2.format(imtValue)));
    }

    private void setupSharedPref() {
        preferenceHelper = PreferenceHelper.getInstance(getContext());
    }

    private void valueBMI(String gender){
        Double weight = Double.parseDouble(preferenceHelper.getString(ConstantPreferences.WEIGHT, "0"));
        Double height = Double.parseDouble(preferenceHelper.getString(ConstantPreferences.HEIGHT, "0"));
        int age = Integer.parseInt(preferenceHelper.getString(ConstantPreferences.UMUR, "0"));
        if (gender.equalsIgnoreCase("Pria")){
            bmr = 88.36 + (13.4*weight)+(4.8*height)-(5.68*age);
            if (imtValue<17){
                tvKategori.setText(ConstantPreferences.KURUS);
                message = "Tambah konsumsi makanan berkalori";
                tvKondisi.setText(ConstantPreferences.MALNUTRISI);
            } else if (imtValue < 24){
                tvKategori.setText(ConstantPreferences.NORMAL);
                message = "Berat badan termasuk ideal";
                tvKondisi.setText(ConstantPreferences.IDEAL);
            } else if (imtValue<28){
                tvKategori.setText(ConstantPreferences.GEMUK);
                message = "Harus waspada";
                tvKondisi.setText(ConstantPreferences.OBESITAS);
            } else {
                tvKategori.setText(ConstantPreferences.SANGAT_GEMUK);
                message = "Sebaiknya memulai program menurunkan berat badan agar lebih ideal";
                tvKondisi.setText(ConstantPreferences.OBESITAS);
            }
        } else {
            bmr = 447.60 +(9.25*weight)+(3.1*height)-(4.30*age);
            if (imtValue<18){
                tvKategori.setText(ConstantPreferences.KURUS);
                tvKondisi.setText(ConstantPreferences.MALNUTRISI);
                message = "Sebaiknya mulai menambah berat badan dan mengkonsumsi makanan berkarbohidrat diimbangi dengan olah raga";
            } else if (imtValue < 26){
                tvKategori.setText(ConstantPreferences.NORMAL);
                message = "Bagus, berat badan termasuk kategori ideal";
                tvKondisi.setText(ConstantPreferences.IDEAL);
            } else if (imtValue<28) {
                tvKategori.setText(ConstantPreferences.GEMUK);
                tvKondisi.setText(ConstantPreferences.OBESITAS);
                message = "Sudah termasuk kategori gemuk, sebaiknya hindari makanan berlemak dan mulai meningkatkan olahraga seminggu minimal 2 kali";

            } else {
                tvKategori.setText(ConstantPreferences.SANGAT_GEMUK);
                tvKondisi.setText(ConstantPreferences.OBESITAS);
                message = "Sebaiknya segera membuat program menurunkan berat badan karena ini termasuk kategori obesitas/terlalu gemuk dan tidak baik bagi kesehatan";
            }
        }
    }

    private void alertDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private void getKalori(){
        int golongan;
        if (args.equalsIgnoreCase("Tidak Aktif")){
            bmr = bmr*1.2;
        } else if (args.equalsIgnoreCase("Cukup Aktif")){
            bmr = bmr*1.375;
        } else if (args.equalsIgnoreCase("Aktif")){
            bmr = bmr*1.55;
        } else {
            bmr = bmr * 1.725;
        }
        tvKalori.setText(String.valueOf(df2.format(bmr)));
        if (bmr<1200){
            golongan = 1;
        } else if (bmr<1401){
            golongan = 2;
        } else if (bmr < 1601){
            golongan = 3;
        } else  if (bmr<1801){
            golongan = 4;
        } else if (bmr<2001){
            golongan = 5;
        } else if (bmr < 2201){
            golongan = 6;
        } else if (bmr<2401){
            golongan = 7;
        } else {
            golongan = 8;
        }
        preferenceHelper.setInt(ConstantPreferences.GOLONGAN, golongan);
    }
}
