package com.ta.doxygen.mealplanner.model;

import com.google.gson.annotations.SerializedName;

public class ResponseInsert {
    @SerializedName("success")
    private Boolean success;

    public Boolean getSuccess() {
        return success;
    }
}
