package com.ta.doxygen.mealplanner.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.ta.doxygen.mealplanner.R;
import com.ta.doxygen.mealplanner.activity.Scan;

public class BluetoothFragment extends Fragment {

    View rootView;
    Button scan;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_bluetooth, container, false);
        scan = (Button) rootView.findViewById(R.id.start_scan);
        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), Scan.class);
                startActivity(i);
            }
        });

        return rootView;
    }
}
