package com.ta.doxygen.mealplanner.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ta.doxygen.mealplanner.R;
import com.ta.doxygen.mealplanner.pref.ConstantPreferences;
import com.ta.doxygen.mealplanner.pref.PreferenceHelper;

public class SplashScreen extends AppCompatActivity {
    private PreferenceHelper preferenceHelper;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        setupSharedPref();
        token = preferenceHelper.getString(ConstantPreferences.TOKEN, ConstantPreferences.TOKEN);

        Thread thread = new Thread() {
            public void run() {
                try {
                    sleep (3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }   finally {
                    if (preferenceHelper.getString(ConstantPreferences.TOKEN, "0").equals("1")){
                        startActivity(new Intent(SplashScreen.this, MainActivity.class));
                    } else {
                        startActivity(new Intent(SplashScreen.this, Login.class));
                    }
                    finish();

                }
            }
        };
        thread.start();
    }

    private void setupSharedPref() {
        preferenceHelper = PreferenceHelper.getInstance(SplashScreen.this);
    }
}
