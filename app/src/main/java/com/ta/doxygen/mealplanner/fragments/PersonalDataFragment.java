package com.ta.doxygen.mealplanner.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ta.doxygen.mealplanner.R;
import com.ta.doxygen.mealplanner.activity.JenisAktivitas;
import com.ta.doxygen.mealplanner.pref.ConstantPreferences;
import com.ta.doxygen.mealplanner.pref.PreferenceHelper;

public class PersonalDataFragment extends Fragment {
    TextView tvNama, tvUmur, tvGender;
    EditText etTinggi, etBerat;
    RadioGroup rbGrup;
    RadioButton rbAktivitas;
    View bCekKalori;
    Button bJenisAktivitas;
    private PreferenceHelper preferenceHelper;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_personaldata, container, false);
        tvNama = view.findViewById(R.id.pdnama);
        tvUmur = view.findViewById(R.id.pdumur);
        tvGender = view.findViewById(R.id.pdgender);
        etTinggi = view.findViewById(R.id.pdtinggi);
        etBerat = view.findViewById(R.id.pdberat);
        rbGrup = view.findViewById(R.id.rbgrup);
        bCekKalori = view.findViewById(R.id.b_cek_kalori);
        bJenisAktivitas = view.findViewById(R.id.b_jenis_aktivitas);
        getActivity().setTitle("Personal Data");
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupSharedPref();
        tvNama.setText(preferenceHelper.getString(ConstantPreferences.NAMA,
                "Nama"));
        tvUmur.setText(preferenceHelper.getString(ConstantPreferences.UMUR, "Umur"));
        tvGender.setText(preferenceHelper.getString(ConstantPreferences.GENDER, "Gender"));
        bJenisAktivitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), JenisAktivitas.class);
                startActivity(intent);
            }
        });

        bCekKalori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedId = rbGrup.getCheckedRadioButtonId();
                if (etBerat.getText().toString().matches("") || etTinggi.getText().toString().matches("")){
                    Toast.makeText(getContext(), "Mohon data dilengkapi", Toast.LENGTH_LONG).show();
                } else if (selectedId != -1){
                    preferenceHelper.setString(ConstantPreferences.HEIGHT, etTinggi.getText().toString());
                    preferenceHelper.setString(ConstantPreferences.WEIGHT, etBerat.getText().toString());
                    rbAktivitas = rbGrup.findViewById(selectedId);
                    preferenceHelper.setString(ConstantPreferences.AKTIVITAS, rbAktivitas.getText().toString());

                    Fragment fragment = PolaMakanFragment.newInstance(rbAktivitas.getText().toString());
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_container, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
                 else {
                    Toast.makeText(getContext(), "Mohon jenis Aktivitas di Cek", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void setupSharedPref() {
        preferenceHelper = PreferenceHelper.getInstance(getContext());
    }
}
