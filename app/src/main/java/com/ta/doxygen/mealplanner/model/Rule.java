package com.ta.doxygen.mealplanner.model;

import android.content.Context;

import com.ta.doxygen.mealplanner.R;
import com.ta.doxygen.mealplanner.pref.ConstantPreferences;

public class Rule {
    private String kategori;
    private String kondisi;
    private double kalori;
    private int golongan;
    private Context context;

    public Rule(Context context, String kondisi, int golongan) {
        this.kondisi = kondisi;
        this.golongan = golongan;
        this.context = context;
    }

    public int getImage(){
        int image = R.drawable.notification_icon_background;
        if (kondisi.equals(ConstantPreferences.NORMAL) && golongan==1){
            image = R.drawable.p1;
        } else if (kondisi.equals(ConstantPreferences.IDEAL) && golongan==2){
            image = R.drawable.p2;
        } else if (kondisi.equals(ConstantPreferences.IDEAL) && golongan==3){
            image = R.drawable.p3;
        } else if (kondisi.equals(ConstantPreferences.IDEAL) && golongan==4){
            image = R.drawable.p4;
        } else if (kondisi.equals(ConstantPreferences.IDEAL) && golongan==5){
            image = R.drawable.p5;
        } else if (kondisi.equals(ConstantPreferences.IDEAL) && golongan==6){
            image = R.drawable.p6;
        } else if (kondisi.equals(ConstantPreferences.IDEAL) && golongan==7){
            image = R.drawable.p7;
        } else if (kondisi.equals(ConstantPreferences.IDEAL) && golongan==8){
            image = R.drawable.p8;
        } else if (kondisi.equals(ConstantPreferences.OBESITAS) && golongan==1){
            image = R.drawable.p9;
        } else if (kondisi.equals(ConstantPreferences.OBESITAS) && golongan==2){
            image = R.drawable.p10;
        } else if (kondisi.equals(ConstantPreferences.OBESITAS) && golongan==3){
            image = R.drawable.p11;
        } else if (kondisi.equals(ConstantPreferences.OBESITAS) && golongan==4){
            image = R.drawable.p12;
        } else if (kondisi.equals(ConstantPreferences.OBESITAS) && golongan==5){
            image = R.drawable.p13;
        } else if (kondisi.equals(ConstantPreferences.OBESITAS) && golongan==6){
            image = R.drawable.p14;
        } else if (kondisi.equals(ConstantPreferences.OBESITAS) && golongan==7){
            image = R.drawable.p15;
        } else if (kondisi.equals(ConstantPreferences.OBESITAS) && golongan==8){
            image = R.drawable.p16;
        } else if (kondisi.equals(ConstantPreferences.MALNUTRISI) && golongan==1){
            image = R.drawable.p17;
        } else if (kondisi.equals(ConstantPreferences.MALNUTRISI) && golongan==2){
            image = R.drawable.p18;
        } else if (kondisi.equals(ConstantPreferences.MALNUTRISI) && golongan==3){
            image = R.drawable.p19;
        } else if (kondisi.equals(ConstantPreferences.MALNUTRISI) && golongan==4){
            image = R.drawable.p20;
        } else if (kondisi.equals(ConstantPreferences.MALNUTRISI) && golongan==5){
            image = R.drawable.p21;
        } else if (kondisi.equals(ConstantPreferences.MALNUTRISI) && golongan==6){
            image = R.drawable.p22;
        } else if (kondisi.equals(ConstantPreferences.MALNUTRISI) && golongan==7){
            image = R.drawable.p23;
        } else if (kondisi.equals(ConstantPreferences.MALNUTRISI) && golongan==8){
            image = R.drawable.p24;
        }
        return image;
    }
}