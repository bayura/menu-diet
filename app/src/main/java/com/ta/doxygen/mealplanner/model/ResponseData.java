package com.ta.doxygen.mealplanner.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseData {
    @SerializedName("result")
    private List<Result> result = null;

    public List<Result> getResult() {
        return result;
    }
}
