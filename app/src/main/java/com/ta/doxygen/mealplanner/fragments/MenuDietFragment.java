package com.ta.doxygen.mealplanner.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.jsibbold.zoomage.ZoomageView;
import com.ta.doxygen.mealplanner.R;
import com.ta.doxygen.mealplanner.pref.ConstantPreferences;
import com.ta.doxygen.mealplanner.pref.PreferenceHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuDietFragment extends Fragment {
    ZoomageView ivDiet;
    PreferenceHelper preferenceHelper;
    FrameLayout flDiet;

    public MenuDietFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_menu_diet, container, false);
        ivDiet = view.findViewById(R.id.iv_menu_diet);
        preferenceHelper = PreferenceHelper.getInstance(getContext());
        flDiet = view.findViewById(R.id.fldiet);
        getActivity().setTitle("Menu Diet");
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ivDiet.setImageResource(Integer.parseInt(preferenceHelper.getString(ConstantPreferences.IMAGE_KEY, String.valueOf(R.drawable.pdefault))));
    }
}
