package com.ta.doxygen.mealplanner.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ta.doxygen.mealplanner.BaseApps;
import com.ta.doxygen.mealplanner.model.ResponseInsert;
import com.ta.doxygen.mealplanner.pref.ConstantPreferences;
import com.ta.doxygen.mealplanner.pref.PreferenceHelper;
import com.ta.doxygen.mealplanner.R;

import retrofit2.Call;
import retrofit2.Callback;


public class Register extends AppCompatActivity {

    TextView tvNama, tvEmail;
    EditText etUmur;
    Button btSave;
    RadioGroup rgGender;
    RadioButton rbGender;
    String gender, umur;
    private PreferenceHelper preferenceHelper;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        setupSharedPref();

        tvNama = findViewById(R.id.rgsnama);
        tvEmail = findViewById(R.id.rgsemail);
        rgGender = findViewById(R.id.rgsgender);
        etUmur = findViewById(R.id.rgsumur);
        btSave = findViewById(R.id.btnsimpan);

        progressDialog = new ProgressDialog(this);

        final String nama = getIntent().getStringExtra("nama");
        final String email = getIntent().getStringExtra("email");
        final String image = getIntent().getStringExtra("image");
        tvNama.setText(nama);
        tvEmail.setText(email);

        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = rgGender.getCheckedRadioButtonId();
                rbGender = findViewById(selectedId);
                umur = etUmur.getText().toString();
                Intent intent = null;
                if (!rbGender.isChecked() || umur.equals("")){
                    Toast.makeText(Register.this, "Mohon data dilengkapi", Toast.LENGTH_LONG).show();
                } else {
                    gender = rbGender.getText().toString();
                    intent = new Intent(Register.this,MainActivity.class);
                    intent.putExtra(ConstantPreferences.KEY, "0");
                    intent.putExtra(ConstantPreferences.NAMA, nama);
                    intent.putExtra(ConstantPreferences.EMAIL, email);
                    intent.putExtra(ConstantPreferences.IMAGE, image);
                    save(nama, email, umur, gender, image);
                    saveUser(nama, email, Integer.parseInt(umur), gender);
                    startActivity(intent);
                    finish();
                }
                progressDialog.dismiss();
            }
        });

    }

    private void save(final String nama, final String email, final String umur, final String gender, String image ) {
        preferenceHelper.setString(ConstantPreferences.NAMA, nama);
        preferenceHelper.setString(ConstantPreferences.EMAIL, email);
        preferenceHelper.setString(ConstantPreferences.UMUR, umur);
        preferenceHelper.setString(ConstantPreferences.GENDER, gender);
        preferenceHelper.setString(ConstantPreferences.IMAGE, image);
        progressDialog.setMessage("Registering user...");
        progressDialog.show();
    }

    private void setupSharedPref() {
        preferenceHelper = PreferenceHelper.getInstance(Register.this);
    }

    private void saveUser(final String nama, final String email, final int umur, final String gender){
        BaseApps.getServices().responseInsert(nama, email, umur, gender).enqueue(new Callback<ResponseInsert>() {
            @Override
            public void onResponse(Call<ResponseInsert> call, retrofit2.Response<ResponseInsert> response) {
                if(response.isSuccessful()){
                    if (response.body().getSuccess()){
                        Toast.makeText(Register.this, "Data Sukses disimpan", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(Register.this, "Gagal", Toast.LENGTH_LONG).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<ResponseInsert> call, Throwable t) {

            }
        });
    }
}
