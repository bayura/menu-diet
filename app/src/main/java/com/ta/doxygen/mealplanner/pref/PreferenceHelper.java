package com.ta.doxygen.mealplanner.pref;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHelper {

    private static PreferenceHelper preferenceHelper;
    private SharedPreferences sharedPreferences;

    public static PreferenceHelper getInstance(Context context) {
        if (null == preferenceHelper) {
            preferenceHelper = new PreferenceHelper(
                    context.getSharedPreferences(ConstantPreferences.KEY, Context.MODE_PRIVATE)
            );
        }
        return preferenceHelper;
    }

    public PreferenceHelper(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public void setString(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    public String getString(String key, String defaultValue) {
        return sharedPreferences.getString(key, defaultValue);
    }

    public void setInt(String key, int defaultValue){
        sharedPreferences.edit().putInt(key, defaultValue).apply();
    }

    public Integer getInt(String key, int defaultValue){
        return sharedPreferences.getInt(key, defaultValue);
    }

    public void clear(String key) {
        sharedPreferences.edit().remove(key).apply();
    }
}
