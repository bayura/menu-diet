package com.ta.doxygen.mealplanner;

import com.ta.doxygen.mealplanner.model.ResponseData;
import com.ta.doxygen.mealplanner.model.ResponseInsert;
import com.ta.doxygen.mealplanner.model.ResponseRead;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    @GET("insert_user.php")
    Call<ResponseInsert> responseInsert(@Query("nama")String nama, @Query("email")String email, @Query("umur")int umur, @Query("gender")String gender);

    @GET("read_user.php")
    Call<ResponseData> responseRead(@Query("nama") String nama, @Query("email")String email);

    @GET("cek_data.php")
    Call<ResponseInsert> cekData(@Query("nama") String nama, @Query("email")String email);
}
