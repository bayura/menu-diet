 package com.ta.doxygen.mealplanner.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelUuid;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.ta.doxygen.mealplanner.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class Scan extends Activity {

    private static final int REQUEST_ENABLE_BT = 1;
    ConnectedThread mConnectedThread;
    BluetoothDevice device;
    ListView listDevicesFound;
    Handler handler;
    TextView stateBluetooth;
    BluetoothAdapter bluetoothAdapter;
    List<String> info = new ArrayList<>();
    List<BluetoothDevice> dvc = new ArrayList<>();
    private
    BluetoothSocket btSocket = null;
    //private ConnectedThread mConnectedThread;
    Vibrator vibrator;
    private int seconds=0;
    public static int send_rata = 0;
    public static int send_max = 0;
    Handler mHandler;
    final int handlerState = 0;
    String dataInPrint;
    StringBuilder recDataString = new StringBuilder();
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    // String for MAC address
    private static String address;
    private boolean startRun;
    Object[] devices;
    BluetoothDevice devicex;
    ParcelUuid[] uuids;

    ArrayAdapter<String> btArrayAdapter;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bluetooth_main);
        stateBluetooth = (TextView) findViewById(R.id.activate);
        stateBluetooth.setOnClickListener(btnScanDeviceOnClickListener);


        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        listDevicesFound = (ListView) findViewById(R.id.lvDevice);
        btArrayAdapter = new ArrayAdapter<String>(Scan.this, android.R.layout.simple_list_item_1);
        listDevicesFound.setAdapter(btArrayAdapter);

        listDevicesFound.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("HandlerLeak")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                bluetoothAdapter.cancelDiscovery();
                //create the bond
                //NOTE: Requires API 17+? I Think this is JellyBean
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    dvc.get(position).createBond();
                    //address = mBTDevices.get(i).getAddress();
                }
                Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();
                if (bondedDevices.size() > 0) {
                    for (BluetoothDevice device : bondedDevices) {
                        if (device.getName().startsWith("HC")) {

                            devices = (Object[]) bondedDevices.toArray();
                            devicex = dvc.get(position);
                            uuids = devicex.getUuids();
                            try {
                                btSocket = devicex.createRfcommSocketToServiceRecord(uuids[0].getUuid());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            try {
                                btSocket.connect();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                if (btSocket!=null)
                mConnectedThread = new ConnectedThread(btSocket);
                else {
                    try {
                        mConnectedThread = new ConnectedThread(createBluetoothSocket(dvc.get(position)));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.e("meal", "BTSocket kosong");
/*
                    Log.e("meal", "devices "+devices.length);
                    Log.e("meal", "devicex "+devicex.getUuids().length);
                    Log.e("meal", "uuids "+uuids.length);
*/

                }
                mConnectedThread.start();


                handler = new Handler();
                startRun = true;
                handler.postDelayed(hitung, 1000);

                mHandler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        byte[] writeBuf = (byte[]) msg.obj;
                        int begin = (int) msg.arg1;
                        int end = (int) msg.arg2;
                        switch (msg.what) {
                            case 1:
                                String writeMessage = new String(writeBuf);
                                writeMessage = writeMessage.substring(begin, end).trim().replaceAll("\n", "");
                                Log.i("kkk", writeMessage);

                                break;
                            default:
                                Log.i("kkk", "error bro");
                        }
                    }
                };


            }
        });
        CheckBlueToothState();

        //btnScanDevice.setOnClickListener(btnScanDeviceOnClickListener);

        registerReceiver(ActionFoundReceiver,
                new IntentFilter(BluetoothDevice.ACTION_FOUND));
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        unregisterReceiver(ActionFoundReceiver);
    }

    private void CheckBlueToothState() {
        if (bluetoothAdapter == null) {
            stateBluetooth.setText("Bluetooth NOT support");
        } else {
            if (bluetoothAdapter.isEnabled()) {
                if (bluetoothAdapter.isDiscovering()) {
                    stateBluetooth.setText("Bluetooth is currently in device discovery process.");
                    stateBluetooth.setEnabled(false);
                } else {
                    stateBluetooth.setText("Bluetooth is Enabled.");
                    stateBluetooth.setEnabled(true);

                }
            } else {
                stateBluetooth.setText("Bluetooth is NOT Enabled!");
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
    }

    private Button.OnClickListener btnScanDeviceOnClickListener
            = new Button.OnClickListener() {

        @Override
        public void onClick(View arg0) {
            // TODO Auto-generated method stub
            btArrayAdapter.clear();
            info.clear();
            bluetoothAdapter.startDiscovery();
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if (requestCode == REQUEST_ENABLE_BT) {
            CheckBlueToothState();
        }
    }

    private final BroadcastReceiver ActionFoundReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("hhh", "On receive");
            // TODO Auto-generated method stub
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                Log.i("hhh", "found");
                device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                dvc.add(device);
                btArrayAdapter.add(device.getName() + "\n" + device.getAddress() + "\n" + device.getUuids());
                info.add(device.getAddress());
                btArrayAdapter.notifyDataSetChanged();
            }
        }
    };

    private AdapterView.OnItemClickListener myListClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView av, View v, int arg2, long arg3) {

            String address = info.get(arg2);
            // Make an intent to start next activity.
            //Intent i = new Intent(Scan.this, ledControl.class);
            //Change the activity.
            //i.putExtra("address", address); //this will be received at ledControl (class) Activity
            //startActivity(i);
        }
    };


    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException
    {
        ParcelUuid[] uuids = device.getUuids();
        return device.createRfcommSocketToServiceRecord(uuids[0].getUuid());
    }



    private class ConnectedThread extends Thread {
        InputStream mmInStream;

        //creation of the connect thread
        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            try {
                btSocket.connect();


            try {
                //Create I/O streams for connection
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e("kkk", e.getMessage());
            }

            mmInStream = tmpIn;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void run() {
            byte[] buffer = new byte[1024];
            int bytes;
            // Keep looping to listen for received messages
            while (true) {
                int begin = 0;
                bytes = 0;
                while (true) {
                    try {
                        bytes += mmInStream.read(buffer, bytes, buffer.length - bytes);
                        for (int i = begin; i < bytes; i++) {
                            if (buffer[i] == "*".getBytes()[0]) {
                                mHandler.obtainMessage(1, begin, i, buffer).sendToTarget();
                                begin = i + 1;
                                if (i == bytes - 1) {
                                    bytes = 0;
                                    begin = 0;
                                }
                            }
                        }
                    } catch (IOException e) {
                        break;
                    }
                }
            }
        }
    }

    private Runnable hitung = new Runnable() {

        @Override
        public void run() {

            handler.postDelayed(this, 1000);

            // TODO Auto-generated method stub

        }
    };


}