package com.ta.doxygen.mealplanner.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;
import com.ta.doxygen.mealplanner.BaseApps;
import com.ta.doxygen.mealplanner.fragments.BluetoothFragment;
import com.ta.doxygen.mealplanner.fragments.MenuDietFragment;
import com.ta.doxygen.mealplanner.model.ResponseData;
import com.ta.doxygen.mealplanner.model.Result;
import com.ta.doxygen.mealplanner.pref.ConstantPreferences;
import com.ta.doxygen.mealplanner.fragments.PersonalDataFragment;
import com.ta.doxygen.mealplanner.fragments.PolaMakanFragment;
import com.ta.doxygen.mealplanner.pref.PreferenceHelper;
import com.ta.doxygen.mealplanner.R;
import com.ta.doxygen.mealplanner.pref.TentangFragment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener {

    private DrawerLayout drawer;
    private static final String TAG = Login.class.getSimpleName();
    private GoogleApiClient mGoogleApiClient;
    private PreferenceHelper preferenceHelper;
    private String  nama, email, image;
    private ImageView ivFoto;
    private TextView tvNama;
    private TextView tvEmail;
    private List<Result> results = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupSharedPref();

        String code = getIntent().getStringExtra(ConstantPreferences.KEY);
        nama = getIntent().getStringExtra("nama");
        email = getIntent().getStringExtra("email");
        image = getIntent().getStringExtra("image");
        preferenceHelper.setString(ConstantPreferences.IMAGE_KEY, String.valueOf(R.drawable.pdefault));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);

        ivFoto = header.findViewById(R.id.iv_profil);
        tvNama = header.findViewById(R.id.tv_nama);
        tvEmail = header.findViewById(R.id.tv_email);

        if (code.equals(ConstantPreferences.TOKEN_KEY)){
            readData(nama, email);
        } else {
            setData();
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment;
        FragmentManager fragmentManager;
        FragmentTransaction fragmentTransaction;
        switch (item.getItemId()) {
            case R.id.nav_bluetooth:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new BluetoothFragment()).commit();
                break;
            case R.id.nav_personaldata:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new PersonalDataFragment()).commit();
                break;
            case R.id.nav_menudiet:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new MenuDietFragment()).commit();
                break;
            case R.id.nav_polamakan:
                fragment = PolaMakanFragment.newInstance(preferenceHelper.getString(ConstantPreferences.AKTIVITAS, "Tidak Aktif"));
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
            case R.id.nav_tentang:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new TentangFragment()).commit();
                break;
            case R.id.nav_logout:
                logoutuser();
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logoutuser() {
        preferenceHelper.clear(ConstantPreferences.NAMA);
        preferenceHelper.clear(ConstantPreferences.EMAIL);
        preferenceHelper.clear(ConstantPreferences.IMAGE);
        preferenceHelper.clear(ConstantPreferences.UMUR);
        preferenceHelper.clear(ConstantPreferences.GENDER);
        preferenceHelper.clear(ConstantPreferences.HEIGHT);
        preferenceHelper.clear(ConstantPreferences.WEIGHT);
        preferenceHelper.clear(ConstantPreferences.AKTIVITAS);
        preferenceHelper.clear(ConstantPreferences.TOKEN);
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
            new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    Intent reLogin = new Intent(getApplicationContext(),Login.class);
                    startActivity(reLogin);
                }
            });
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
        super.onBackPressed();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void setupSharedPref() {
        preferenceHelper = PreferenceHelper.getInstance(MainActivity.this);
    }

    private void setData(){
        tvNama.setText(preferenceHelper.getString(ConstantPreferences.NAMA,
                "Nama"));
        tvEmail.setText(preferenceHelper.getString(ConstantPreferences.EMAIL,
                "EMAIL"));
        Picasso.get().load(preferenceHelper.getString(ConstantPreferences.IMAGE, "image")).into(ivFoto);
    }

    private void save(final String nama, final String email, final String umur, final String gender, String image ) {
        preferenceHelper.setString(ConstantPreferences.NAMA, nama);
        preferenceHelper.setString(ConstantPreferences.EMAIL, email);
        preferenceHelper.setString(ConstantPreferences.UMUR, umur);
        preferenceHelper.setString(ConstantPreferences.GENDER, gender);
        preferenceHelper.setString(ConstantPreferences.IMAGE, image);;
        preferenceHelper.setString(ConstantPreferences.TOKEN, "1");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        preferenceHelper.clear(ConstantPreferences.HEIGHT);
        preferenceHelper.clear(ConstantPreferences.WEIGHT);
    }

    public void readData(final String nama, final String email){
        BaseApps.getServices().responseRead(nama, email).enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful()){
                    results.addAll(response.body().getResult());
                    save(nama, email, results.get(0).umur, results.get(0).gender, image);
                    setData();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error : "+t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
