package com.ta.doxygen.mealplanner.model;

import com.google.gson.annotations.SerializedName;

public class ResponseRead {
    @SerializedName("nama")
    private String nama;
    @SerializedName("email")
    private String email;
    @SerializedName("umur")
    private String umur;
    @SerializedName("gender")
    private String gender;

    public String getNama() {
        return nama;
    }

    public String getEmail() {
        return email;
    }

    public String getUmur() {
        return umur;
    }

    public String getGender() {
        return gender;
    }
}
